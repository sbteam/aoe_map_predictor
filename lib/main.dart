import 'package:flutter/material.dart';
import 'package:matrix4_transform/matrix4_transform.dart';
import 'query.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'AOE Map Predictor',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.indigo,
      ),
      home: Center(child: MyHomePage(title: 'AOE Map Predictor')),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  var cornerText = {0:"All", 1:"1h", 2:"2h", 3:"3h", 4:"4h", 5:"5h", 6:"6h", 7:"7h", 8:"8h", 9:"9h", 10:"10h", 11:"11h", 12:"12h"};

  int _counter = 0;
  int _currentCornerValue = 0;
  Map<int, bool> _bushes = {};
  List<dynamic> _listMaps = [];

  String zeroBerries(Map<int, bool> berries) {

    var minX = 8;
    var minY = 8;

    for (int i = 0; i<7; i++) {
      for (int j=0; j<7; j++) {
        if (berries[(7-j-1)*7+i]) {
          if (i<minX) {
            minX = i;
          }
          if (i<minY) {
            minY = j;
          }
        }
      }
    }

    var res = "";
    for (int i = 0; i<7; i++) {
      for (int j=0; j<7; j++) {
        if (berries[(7-j-1)*7+i]) {
          res = res + (i-minX).toString() + '.0@' + (j-minY).toString() + '.0;';
        }
      }
    }
    return res.substring(0, res.length-1);
  }

  void _getMaps() {

    debugPrint("map corner = " + _currentCornerValue.toString());

    int _mapStart = 0;
    int _mapEnd = 360;
    if (_currentCornerValue != 0) {
      _mapStart = 15*(2*_currentCornerValue-1)-1;
      if (_currentCornerValue!= 12) {
        _mapEnd = 15*(2*_currentCornerValue+1)+1;
      } else {
        _mapEnd = 16;
      }
    }

    String zero = zeroBerries(_bushes);

    Future<MapQuery> mapsQuery = MapQuery.postMaps(zero, _mapStart, _mapEnd);
    mapsQuery.then((data) {
      if (data.toString() != _listMaps.toString()) {
        debugPrint(data.maps.toString());
        _listMaps = data.maps;
        _counter = _listMaps.length;
        debugPrint(_listMaps.toString());
        setState(() {});
      }
    });
  }

  BoxDecoration myBoxDecoration() {
    return BoxDecoration(
      border: Border.all(),
    );
  }

  void _onBushClicked(int index){
    setState(() {
      _bushes[index] = !_bushes[index];
      debugPrint("You tapped on item $index");
    });
  }

  Color getColor(int index){
    _bushes[index] ??= false; // If null then false

    if (_bushes[index]) {
      return Colors.green;
    } else {
      return Colors.black12;
    }
  }

  BoxDecoration getDecor(int index){
    _bushes[index] ??= false; // If null then false
    if (_bushes[index]) {
      return new BoxDecoration(
        border: Border.all(color: Colors.black12),
        image: new DecorationImage(
          image: new AssetImage("assets/images/bush.png"),
          fit: BoxFit.cover,),
      );
    } else {
      return new BoxDecoration(
        border: Border.all(color: Colors.black12),
      );
    }
  }

  List<Widget> _prepareBushes() {
    final List<Widget> tiles = <Widget>[];
    for (int i = 0; i < 49; i++) {
      tiles.add( new Container(
        decoration: getDecor(i),
        child: new GridTile(
            child: new InkResponse(
              enableFeedback: true,
              child: Text('',),
              onTap: () => _onBushClicked(i),
            ))),
      );
    }
    return tiles;
  }

  @override
  Widget build(BuildContext context) {

    var mediaQueryData = MediaQuery. of (context);
    final double widthScreen = mediaQueryData.size.width;

    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: () {
              debugPrint("settings pressed");
            },
          )
        ],
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
//          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Transform(
              transform: Matrix4Transform().scaleVertically(0.7).matrix4,
              child: Container(
                child: Transform.scale(
                  scale: 0.7,
                  child: RotationTransition(
                    turns: new AlwaysStoppedAnimation(45 / 360),
                    child: GridView.count(
                      crossAxisCount: 7,
                      shrinkWrap: true,
                      children: _prepareBushes(),
                    ),
                  ),
                ),
              ),
            ),
            Row(
              children: <Widget>[
                Text("Map corner "),
                DropdownButton<int>(
                  items: <int>[0,1,2,3,4,5,6,7,8,9,10,11,12].map((int val) {
                    return new DropdownMenuItem<int>(
                      value: val,
                      child: new Text(cornerText[val]),
                    );
                  }).toList(),
                  value: _currentCornerValue,
                  onChanged: (int value) {
                    setState(() {
                      _currentCornerValue = value;
                    });
                  },
                ),
              ],
            ),
            Text('found $_counter maps'
            ),
            Container(
              height: 120,
              child : ListView(
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                children: List.generate(_listMaps.length, (index) {
                  return Center(
                    child: MapCard(_listMaps[index]),
                  );
                }),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _getMaps,
        tooltip: 'Get maps',
        child: Icon(Icons.search),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
