import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:photo_view/photo_view.dart';

class MapQuery {

  String berries;
  String query;
  List<dynamic> maps;

  MapQuery(this.berries, this.query, this.maps);

  static Future<MapQuery> postMaps(String zeroBush, int mapStart, int mapEnd) async {
    String body = "{\"berries\":\"" + zeroBush + "\"}";

    debugPrint(body);

    FormData formData = FormData.fromMap({"berries" : zeroBush, "map_start" : mapStart, "map_end" : mapEnd});
//    FormData formData = FormData.fromMap({"berries" : zeroBush});

    Response resp = await Dio().post(
        'http://sbsvn.net/apps/aoe/search.php',
      data: formData);

    return fromJson(jsonDecode(resp.toString()));
  }

  static MapQuery fromJson(Map<String, dynamic> json) {
    return MapQuery(json['berries'], json['query'], json['maps']);
  }

}

class MapCard extends StatelessWidget {
  String scxid;
  MapCard(this.scxid);

  @override
  Widget build(BuildContext context) {
    return InkWell(
          child: Card(
            child: Column(
              children: <Widget>[
                new Container(
                  child: Image.network("http://sbsvn.net/apps/aoe/view.php?seedid=" + scxid + "&num_player=2&map_type=4&scale=1&show=berrydeerelephantlionstonegold"),
                )
              ],
            ),
          ),
      onTap: () => _onMapPressed(context, scxid),
      );
  }

  _onMapPressed(BuildContext context, String id) {
    debugPrint("id map = " + id.toString());
    showDialog(
        context: context,
        builder: (BuildContext context) {
        // return object of type Dialog
          return AlertDialog(
            contentPadding: EdgeInsets.all(0.0),
            content: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.width,
              child: PhotoView(
                imageProvider: NetworkImage("http://sbsvn.net/apps/aoe/view.php?seedid=" + id + "&num_player=2&map_type=4&scale=10&show=berrydeerelephantlionstonegold")),
              ),
          );
          }
    );
  }
}